const mysql = require("mysql");
const util = require("util");
const sqlString = require("sqlstring");


async function conectar() {
    if (!global.conexion) {
        global.conexion = mysql.createPool({
            connectionLimit: 5,
            host: global.config.db_host,
            user: global.config.db_user,
            password: global.config.db_password,
            database: global.config.db_database
        });
        global.conexion.query = util.promisify(global.conexion.query);
    }
    return global.conexion;
}

async function listar() {
    const conexion = await conectar();
    try {
        var resultados = await conexion.query('select * from contacto');
        return resultados
    } catch (err) {
        throw err
    }
}

async function obtenerUno(id) {
    const conexion = await conectar();
    try {
        const sql = sqlString.format('select * from contacto where id=?', [id]);
        var resultados = await conexion.query(sql);
        return resultados[0];
    } catch (err) {
        throw err;
    }
}

async function agregar(contacto) {
    const conexion = await conectar();
    try {
        const sql = sqlString.format('insert into contacto (nombre, apellido, telefono, email) values (?,?,?,?)', [contacto.nombre, contacto.apellido, contacto.telefono, contacto.email]);
        var resultados = await conexion.query(sql);
        return resultados;
    } catch (err) {
        throw err;
    }
}

async function actualizar(contacto) {
    const conexion = await conectar();
    try {
        const sql = sqlString.format(
            "update contacto set nombre=?, apellido=?, telefono=?, email=? where id=?",
            [contacto.nombre, contacto.apellido, contacto.telefono, contacto.email, contacto.id]
        );
        var resultados = await conexion.query(sql);
        return resultados;
    } catch (err) {
        throw err;
    }
}

async function borrar(id) {
    const conexion = await conectar();
    try {
        const sql = sqlString.format(
            "delete from contacto where id=?",
            [id]
        );
        var resultados = await conexion.query(sql);
        return resultados;
    } catch (err) {
        throw err;
    }
}

async function crearEstructuraDatos() {
    const conexion = mysql.createConnection({
        host: global.config.db_host,
        user: global.config.db_user,
        password: global.config.db_password
    });
    await conexion.query('DROP DATABASE IF EXISTS app_deploy ');
    await conexion.query('CREATE DATABASE app_deploy ');
    await conexion.query('use app_deploy');
    await conexion.query('CREATE TABLE contacto (id int(11) NOT NULL AUTO_INCREMENT,nombre varchar(50) NOT NULL,apellido varchar(50) NOT NULL,telefono varchar(30) NOT NULL,email varchar(30) NOT NULL,PRIMARY KEY(id)) ENGINE = InnoDB DEFAULT CHARSET = latin1;');
    await conexion.query('insert into contacto (nombre, apellido, telefono, email) values ("nombre 1", "apellido 1", "1233443311", "prueba@gmail.com")')
}

module.exports.listar = listar;
module.exports.agregar = agregar;
module.exports.obtenerUno = obtenerUno;
module.exports.actualizar = actualizar;
module.exports.borrar = borrar;

module.exports.crearEstructuraDatos = crearEstructuraDatos;