const app = require('express')()
const bodyParser = require('body-parser')
const exphbs = require("express-handlebars");
require('express-async-errors');

const model = require('./models/model_contacto');

app.use(bodyParser.urlencoded({ extended: false }));
app.engine(".hbs", exphbs({ defaultLayout: "default", extname: ".hbs" }));
app.set("view engine", ".hbs");

// Config
const config = require("./config.json");
const environment = process.env.NODE_ENV || "development";
global.config = config[environment];

app.get('/', async (req, res) => {
    let resultados = await model.listar();
    res.render("home", { listado: resultados });
});

app.post("/", async (req, res) => {
    let contacto = {};
    contacto.nombre = req.body.nombre;
    contacto.apellido = req.body.apellido;
    contacto.telefono = req.body.telefono;
    contacto.email = req.body.email;
    await model.agregar(contacto);
    res.redirect("/");
});

app.get("/:id/editar", async (req, res) => {
  let resultados = await model.listar();
  let objEdicion = await model.obtenerUno(req.params.id);
  res.render("home", { listado: resultados, objEdidion: objEdicion });
});


app.post("/:id/editar", async (req, res) => {
    let contacto = {};
    contacto.id = req.params.id;
    contacto.nombre = req.body.nombre;
    contacto.apellido = req.body.apellido;
    contacto.telefono = req.body.telefono;
    contacto.email = req.body.email;
    await model.actualizar(contacto);
    res.redirect("/");
});

app.get('/:id/borrar', async (req, res) => {
    await model.borrar(req.params.id);
    res.redirect("/");
})


app.get('/setup', async (req, res) => {
    await model.crearEstructuraDatos();
    res.send('ok');
})

app.use(function (err, req, res, next) {
    mensajeError = '';
    console.log(err);
    if (err.code=='ER_ACCESS_DENIED_ERROR') {
        mensajeError = 'Los datos de conexión con al base de datos no son correctos';
    }
    res.status(500).render('error', { error: mensajeError, layout: false});
});

app.listen(global.config.app_port, () => {
    console.log(`Escuchando en el puerto ${global.config.app_port}`);
})