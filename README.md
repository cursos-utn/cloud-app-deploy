# cloud-app-deploy

## Configuración

Para correr la aplicación en el servidor, previamente se debe modificar el archivo config.json modificando los parámetros necesarios para correr la aplicación en tus equipos AWS/Google Cloud.


## AWS EC2

### RDS

Debemos crear una instancia de base de datos, puede ser MySQL o MariaDB. Es importante resguardar los datos de conexión a la base de datos, ya que los necesitaremos para configurar la aplicación (archivo config.json)

### Corriendo en una máquina virtual

Descargamos la aplicación de ejemplo

1. mkdir app
2. cd app
3. wget https://s3.amazonaws.com/cursos-utn/cloud/app_deploy.zip
4. unzip app_deploy.zip
5. yum install git

Editamos el archivo de configuración config.json e incorporamos los datos de conexión a la base de datos creada en el punto anterior

Previamente debemos instalar Node JS para correr esta aplicación

1. Debemos conectarnos a la máquina EC2 por medio de SSH (Putty)
2. Ejecutamos ```curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash```
3. Ejecutamos ```. ~/.nvm/nvm.sh```
4. Ejecutamos ```nvm install <version de node a instalar>```

Debemos instalar las dependencias de la aplicación

1. Ejecutamos ```yum install git```
2. Ejecutamos ```npm install```

Debemos instalar PM2 (recomendado) para ello necesitamos ser root

```sudo su```
```. /home/<usuario>/.nvm/nvm.sh```

Para iniciar la aplicación por medio de línea de comandos se necesita tener instalado pm2 (```npm install -g pm2```)

Para correr la aplicación desde línea de comandos 


```./iniciar.sh```

## Google Cloud

### Database

Debemos crear una base de datos MySQL

### Compute Engine 


Descargamos la aplicación de ejemplo

1. mkdir app
2. cd app
3. sudo yum install wget
4. wget https://s3.amazonaws.com/cursos-utn/cloud/app_deploy.zip
5. sudo yum install unzip
6. unzip app_deploy.zip

Editamos el archivo de configuración config.json e incorporamos los datos de conexión a la base de datos creada en el punto anterior

Previamente debemos instalar Node JS para correr esta aplicación

1. Debemos conectarnos a la máquina EC2 por medio de SSH (Putty)
2. Ejecutamos ```curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash```
3. Ejecutamos ```. ~/.nvm/nvm.sh```
4. Ejecutamos ```nvm install <version de node a instalar>```

Debemos instalar las dependencias de la aplicación

1. Ejecutamos ```sudo yum install git```
1. Ejecutamos ```npm install```

Debemos instalar PM2 (recomendado) para ello necesitamos ser root

```sudo su```
```. /home/ec2-user/.nvm/nvm.sh```

Para iniciar la aplicación por medio de línea de comandos se necesita tener instalado pm2 (```npm install -g pm2```)

Para correr la aplicación desde línea de comandos 


```./iniciar.sh```




### Creación de la base de datos

Se debe acceder a la ruta http://<url>/setup para inicializar la base de datos y tabla del proyecto. Es importante previamente haber configurado correctamente el archivo config.json