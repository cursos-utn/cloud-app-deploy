module.exports = {
    apps: [
        {
            name: "CursoCloud",
            script: "./app.js",
            watch: true,
            instance_var: 'INSTANCE_ID',
            env: {
                "NODE_ENV": "production"
            }
        }
    ]
}